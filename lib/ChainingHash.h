/*
 *  Separate chaining hashtable
 */

#ifndef __CHAINING_HASH_H
#define __CHAINING_HASH_H

// Standard library includes
#include <iostream>
#include <vector>
#include <list>
#include <stdexcept>

// Custom project includes
#include "Hash.h"

// Namespaces to include
using std::vector;
using std::list;
using std::pair;

using std::cout;
using std::endl;

//
// Separate chaining based hash table - derived from Hash
//
template<typename K, typename V>
class ChainingHash : public Hash<K,V> {
private:
    vector<list<pair<K,V>>> data;   
    int tableSize;
    int insertions; 

public:
    ChainingHash(int n) {
        tableSize = n;  //Init variables
        insertions = 0;

        data.resize(n); //Resize data to size of initial table
        pair<int, int> base;
        base.first = 0;
        base.second = 0;
        for (int x = 0; x < tableSize; x++) {
            data.at(x).push_front(base);    //Insert empty base into each index
        }
    }

    ~ChainingHash() {
        this->clear();
    }

    int size() {
        return insertions;
    }

    V operator[](const K& key) {
        int index = hash(key);  //finds the index
        list<pair<K,V>> tempList = data.at(index);  //Creates temp list at index
        while (tempList.front().first != key) { //While the first element of the list at the index is not the key
            tempList.pop_front();               //pop front element
        }
        return tempList.front().second; //Once element is found, return it
    }

    bool insert(const std::pair<K, V>& pair) {
        int index = hash(pair.first);   //Find the index
        data.at(index).push_front(pair); //Push to the front of the list
        insertions++;   //Iterate insertions
        if (load_factor() > .5) {   //If load factor is too high, rehash
            rehash();
        }
        return true;
    }

    void erase(const K& key) {
        int index = hash(key); //find the index
        list<pair<K,V>> temp; //Init a temp list
        int z = temp.size(); //Get the temp list's size
        for (int x = 0; x < z; x++) {   //Needed for for loop -> temp.size() changes throughout loop, which could cause unintended issues
            if (data.at(index).front().first != key) {  //If the value is not the key to be erased (only entries we want to keep)
                temp.push_front(data.at(index).front());    //Push the entry onto the temp array
                data.at(index).pop_front();                 //Pop off of the data list
            }
        }
        data.at(index) = temp;  //Once loop ends (creates a temp list with all the entries we want to keep), set the data index to the temp list
    }

    void clear() {
        data.clear();
    }

    int bucket_count() {
        return tableSize;
    }

    float load_factor() {
        return (float)insertions/ (float)bucket_count();
    }

private:
    int findNextPrime(int n)
    {
        while (!isPrime(n))
        {
            n++;
        }
        return n;
    }

    int isPrime(int n)
    {
        for (int i = 2; i <= sqrt(n); i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }

        return true;
    }

    int hash(const K& key) {
        return key % bucket_count();       
    }

    void rehash() { //Same as probingHash's rehash
        int newSize = findNextPrime(tableSize + 1);
        tableSize = newSize;
        data.resize(tableSize);
    }

};

#endif //__CHAINING_HASH_H
