#ifndef __PROBING_HASH_H
#define __PROBING_HASH_H

#include <vector>
#include <stdexcept>

#include "Hash.h"

using std::vector;
using std::pair;

// Can be used for tracking lazy deletion for each element in your table
enum EntryState {
    EMPTY = 0,
    VALID = 1,
    DELETED = 2
};

template<typename K, typename V>
class ProbingHash : public Hash<K,V> { // derived from Hash
private:
    // Needs a table and a size.
    // Table should be a vector of std::pairs for lazy deletion
    int tableSize;  //Define some variables
    int insertions;
    vector<pair<K,V>> data;

public:

    ProbingHash(int n) {
        tableSize = n;  //Init variables
        insertions = 0;

        data.resize(n); //Resize table to base size
        pair<int, int> base;
        base.first = 0;
        base.second = 0;
        for (int x = 0; x < tableSize; x++) {
            data.emplace_back(base);    //Insert empty pair into each entry
        }
    }

    ~ProbingHash() {
        this->clear();
    }

    int size() {    //Returns the amount of entries currently in the table
        return insertions;
    }

    V operator[](const K& key) {    
        int i = 0;  //Init probing i
        int index = hash(key) + i;  //Index is set to the hash of key + i
        while (key != data.at(index).first) {   //While the accessed index is not the same as the key
            i = i + 1;                          //iterate i (linear probe)
            index = hash(key) + i;
        }
        return data.at(index).second;           //return
        
    }

    bool insert(const std::pair<K, V>& pair) {
        int i = 0;  //Init probing i
        int index = hash(pair.first) + i; //Init index

        while(data.at(index).first != 0) {
            i = i + 1;  //While the accessed index is not the same as the key, iterate i
            index = hash(pair.first) + i;
            if (index > tableSize) {    //If the index reaches the end of the table, loop to the front of the table
                index = 0;
                i = 0;
                while(data.at(index).first != 0) {
                    index = index + i;
                    i = i + 1;
                }
            }
        }
        insertions++;   //Once a spot has been found, iterate insertions and insert value
        data[index] = pair;
        if (load_factor() > .5) {   //If the load factor is low, rehash
            this->rehash();
        }
        
        return true;
    }

    void erase(const K& key) {
        pair<K,V> ePair;    //Create a blank pair
        ePair.first = 0;
        ePair.second = 0;
        int i = 0; //Set up i and index
        int index = hash(key) + i;

        while(data.at(index).first != 0) { //Search for index using same system as earlier
            i = i + 1;
            index = hash(key) + i;
        }

        data[index] = ePair;    //Set the index to a blank pair
    }

    void clear() {
        data.clear();   //Call vector's clear() function
    }

    int bucket_count() {
        return tableSize;  
    }

    float load_factor() {   
        return  (float)insertions / (float)bucket_count();
    }

private:
    int findNextPrime(int n)
    {
        while (!isPrime(n))
        {
            n++;
        }
        return n;
    }

    int isPrime(int n)
    {
        for (int i = 2; i <= sqrt(n); i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }

        return true;
    }

    int hash(const K& key) {    //Basic hash function
        return key % bucket_count();       
    }

    void rehash() { //Rehash function
        int newSize = findNextPrime(tableSize + 1); //Get a newsize
        tableSize = newSize;    //Change tablesize
        data.resize(tableSize); //Resize

    }
};

#endif //__PROBING_HASH_H
